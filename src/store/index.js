import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import axios from "axios";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    statusLogin: "",
    username: "",
    accessToken: "",
    refreshToken: "",
  },
  getters: {
    statusLogin: (state) => state.statusLogin,
    username: (state) => state.username,
    accessToken: (state) => state.accessToken,
    refreshToken: (state) => state.refreshToken,
    message: (state) => state.message,
  },
  mutations: {
    setStatus(state, statusLogin) {
      state.statusLogin = statusLogin;
      localStorage.setItem("statusLogin", statusLogin);
    },
    setUsername(state, username) {
      state.username = username;
      localStorage.setItem("username", username);
    },
    accessToken(state, accessToken) {
      state.accessToken = accessToken;
      localStorage.setItem("accessToken", accessToken);
    },
    refreshToken(state, refreshToken) {
      state.refreshToken = refreshToken;
      localStorage.setItem("refreshToken", refreshToken);
    },
    setMessage(state, message) {
      state.message = message;
      localStorage.setItem("message", message);
    },
  },
  actions: {
    async login({ commit }, payload) {
      try {
        const response = await axios.post("login", payload);
        if (response.data.statusCode == 10000) {
          commit("setStatus", "lmao");
          commit("setUsername", response.data.data.username);
          commit("accessToken", response.data.data.accessToken);
          commit("refreshToken", response.data.data.refreshToken);
          commit("setMessage", "Success");
          document.cookie = `refreshToken=${response.data.data.refreshToken}; SameSite=Strict; Path=/`;
        } else {
          throw new Error(response.data.data.message);
        }
      } catch (error) {
        const message = error?.response ? error.response.data.message : error;
        commit("setStatus", "error");
        commit("setUsername", "");
        commit("accessToken", "");
        commit("refreshToken", "");
        commit("setMessage", message);
      }
    },
    async register({ commit }, payload) {
      try {
        const response = await axios.post("register", payload);
        if (response.data.statusCode == 10000) {
          commit("setStatus", "lmao");
          commit("setUsername", response.data.data.username);
          commit("accessToken", response.data.data.accessToken);
          commit("refreshToken", response.data.data.refreshToken);
          commit("setMessage", "Success");
          document.cookie = `refreshToken=${response.data.data.refreshToken}; SameSite=Strict; Path=/`;
        } else {
          throw new Error(response.data.data.message);
        }
      } catch (error) {
        const message = error?.response ? error.response.data.message : error;
        commit("setStatus", "error");
        commit("setUsername", "");
        commit("accessToken", "");
        commit("refreshToken", "");
        commit("setMessage", message);
      }
    },
    async logout({ commit }) {
      await axios.post("logout", {
        refreshToken: this.state.refreshToken,
      });
      commit("setStatus", "");
      commit("setUsername", "");
      commit("accessToken", "");
      commit("refreshToken", "");
      commit("setMessage", "");
      document.cookie = `refreshToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
    },
    async updateAccessToken({ commit }, newAccessToken) {
      commit("accessToken", newAccessToken);
    },
  },
  plugins: [createPersistedState()],
  modules: {},
});
