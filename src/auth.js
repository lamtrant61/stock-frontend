import axios from "axios";
import store from "./store/index.js";
import Cookies from "js-cookie";

axios.defaults.headers.common = {
  Authorization: `Bearer ${store.getters.accessToken}`,
};
axios.interceptors.response.use(async function (response) {
  const newAccessToken = Cookies.get("newAccessToken");
  if (newAccessToken) {
    axios.defaults.headers.common = {
      Authorization: `Bearer ${newAccessToken}`,
    };
    await store.dispatch("updateAccessToken", newAccessToken);
  }
  return response;
});

export default axios;
