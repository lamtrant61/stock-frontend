import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import axios from "./auth";
import SocketPlugin from "./socket";

import "buefy/dist/buefy.css";
import Buefy from "buefy";
import VModal from "vue-js-modal";
import VueSimpleAlert from "vue-simple-alert";

import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/css/fontawesome.css";
library.add(fas);

Vue.component("vue-fontawesome", FontAwesomeIcon);
Vue.use(VueSimpleAlert);
Vue.use(VModal);
Vue.use(Buefy, {
  defaultIconComponent: "vue-fontawesome",
  defaultIconPack: "fas",
  // ...
});

Vue.use(SocketPlugin); // Use the socket plugin
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: function (h) {
    return h(App);
  },
}).$mount("#app");
