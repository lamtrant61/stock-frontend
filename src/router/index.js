import Vue from "vue";
import VueRouter from "vue-router";
import HomePage from "../views/HomePage.vue";
import LoginPage from "../views/LoginPage.vue";
import RegisterPage from "../views/RegisterPage.vue";
import UserPage from "../views/UserPage.vue";
import ManagePage from "../views/ManagePage.vue";
import ChartPredict from "../views/ChartPredict.vue";
import ImagePage from "../views/ImagePage.vue";
import ChartRealTime from "../views/ChartRealTime.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomePage,
  },
  {
    path: "/login",
    name: "login",
    component: LoginPage,
  },
  {
    path: "/register",
    name: "register",
    component: RegisterPage,
  },
  {
    path: "/chartpredict",
    name: "chartpredict",
    component: ChartPredict,
  },
  {
    path: "/userinfo",
    name: "UserPage",
    component: UserPage,
  },
  {
    path: "/manage",
    name: "ManagePage",
    component: ManagePage,
  },
  {
    path: "/image",
    name: "ImagePage",
    component: ImagePage,
  },
  {
    path: "/chartrealtime",
    name: "ChartRealTime",
    component: ChartRealTime,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
