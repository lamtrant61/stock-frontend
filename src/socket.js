import Vue from "vue";
import { io } from "socket.io-client";

const socket = io("http://localhost:3000", {
  transports: ["websocket"],
});

Vue.prototype.$socket = socket; // Set $socket as a global property

export default socket;
